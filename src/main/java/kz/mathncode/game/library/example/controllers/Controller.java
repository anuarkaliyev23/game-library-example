package kz.mathncode.game.library.example.controllers;

import io.javalin.http.Context;
import kz.mathncode.game.library.example.model.Model;

public interface Controller<T extends Model<U>, U> {

    void getOne(Context context, U id);
    void getAll(Context context);
    void postOne(Context context);
    void patchOne(Context context, U id);
    void deleteOne(Context context, U id);
}
