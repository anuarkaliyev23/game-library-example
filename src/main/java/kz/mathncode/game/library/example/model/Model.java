package kz.mathncode.game.library.example.model;

public interface Model<T> {
    void setId(T id);
    T getId();
}
