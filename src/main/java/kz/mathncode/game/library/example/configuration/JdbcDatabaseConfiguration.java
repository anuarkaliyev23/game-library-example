package kz.mathncode.game.library.example.configuration;

import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import kz.mathncode.game.library.example.exceptions.ApplicationException;
import kz.mathncode.game.library.example.model.Game;
import kz.mathncode.game.library.example.model.User;
import kz.mathncode.game.library.example.model.UserGame;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;

/**
 * Base class implementing {@link DatabaseConfiguration}
 * It should accepts single JDBC Connection String for
 * connection for database.
 * */
public class JdbcDatabaseConfiguration implements DatabaseConfiguration {
    private static final Logger LOGGER = LoggerFactory.getLogger(JdbcDatabaseConfiguration.class);

    /**
     * Base property for {@link ConnectionSource}
     * */
    private final ConnectionSource connectionSource;

    /**
     * Base constructor
     *
     * @param jdbcConnectionString - simple connection string to build {@link ConnectionSource} from
     * */
    public JdbcDatabaseConfiguration(String jdbcConnectionString) {
        try {
            connectionSource = new JdbcConnectionSource(jdbcConnectionString);
            LOGGER.debug(String.format("JdbcConnectionSource is build from {%s}", jdbcConnectionString));
            createTables();
        } catch (SQLException throwables) {
            LOGGER.error(ExceptionUtils.getStackTrace(throwables));
            throw new ApplicationException(String.format("Couldn't construct connection source from {%s}", jdbcConnectionString));
        }
    }

    private void createTables() throws SQLException {
        TableUtils.createTable(connectionSource, User.class);
        TableUtils.createTable(connectionSource, Game.class);
        TableUtils.createTable(connectionSource, UserGame.class);
    }

    @Override
    public ConnectionSource connectionSource() {
        return connectionSource;
    }
}
