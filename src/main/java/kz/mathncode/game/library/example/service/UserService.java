package kz.mathncode.game.library.example.service;

import com.j256.ormlite.dao.Dao;
import kz.mathncode.game.library.example.model.User;

public class UserService extends AbstractService<User, Integer> {
    public UserService(Dao<User, Integer> dao) {
        super(dao);
    }
}
