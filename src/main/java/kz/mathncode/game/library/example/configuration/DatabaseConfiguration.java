package kz.mathncode.game.library.example.configuration;

import com.j256.ormlite.support.ConnectionSource;

/**
 * Base class for all Configuration classes
 * */
public interface DatabaseConfiguration {

    /**
     * Main interface method
     *
     * @return {@link ConnectionSource} for a given configuration
     * */
    ConnectionSource connectionSource();
}
