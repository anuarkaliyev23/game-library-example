package kz.mathncode.game.library.example;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.j256.ormlite.dao.DaoManager;
import io.javalin.Javalin;
import io.javalin.core.security.Role;
import io.javalin.http.Context;
import io.javalin.http.ForbiddenResponse;
import io.javalin.http.UnauthorizedResponse;
import kz.mathncode.game.library.example.configuration.DatabaseConfiguration;
import kz.mathncode.game.library.example.configuration.JdbcDatabaseConfiguration;
import kz.mathncode.game.library.example.controllers.Controller;
import kz.mathncode.game.library.example.controllers.GameController;
import kz.mathncode.game.library.example.controllers.UserController;
import kz.mathncode.game.library.example.controllers.UserGameController;
import kz.mathncode.game.library.example.json.deserialization.UserDeserializer;
import kz.mathncode.game.library.example.model.Game;
import kz.mathncode.game.library.example.model.User;
import kz.mathncode.game.library.example.model.UserGame;
import kz.mathncode.game.library.example.model.UserRole;
import kz.mathncode.game.library.example.service.GameService;
import kz.mathncode.game.library.example.service.Service;
import kz.mathncode.game.library.example.service.UserGameService;
import kz.mathncode.game.library.example.service.UserService;
import org.mindrot.jbcrypt.BCrypt;

import java.sql.SQLException;

import static io.javalin.apibuilder.ApiBuilder.*;
import static io.javalin.core.security.SecurityUtil.roles;

public class Main {
    public static Role getRole(Context context, Service<User, Integer> userService) {
        if (context.basicAuthCredentialsExist()) {
            String login = context.basicAuthCredentials().getUsername();
            String password = context.basicAuthCredentials().getPassword();
            User actor = userService.findByColumnUnique("login", login);
            if (BCrypt.checkpw(password, actor.getPassword())) {
                return actor.getRole();
            } else {
                throw new UnauthorizedResponse();
            }
        } else {
            throw new UnauthorizedResponse();
        }
    }
    public static void main(String[] args) throws SQLException {
        DatabaseConfiguration databaseConfiguration = new JdbcDatabaseConfiguration("jdbc:sqlite::memory:");
        Service<User, Integer> userService = new UserService(DaoManager.createDao(databaseConfiguration.connectionSource(), User.class));
        Service<Game, Integer> gameService = new GameService(DaoManager.createDao(databaseConfiguration.connectionSource(), Game.class));
        Service<UserGame, Integer> userGameService = new UserGameService(DaoManager.createDao(databaseConfiguration.connectionSource(), UserGame.class));

        ObjectMapper objectMapper = new ObjectMapper()
                .registerModule(
                        new SimpleModule()
                                .addDeserializer(User.class, new UserDeserializer())
                );

        Controller<User, Integer> userController = new UserController(userService, objectMapper);
        Controller<Game, Integer> gameController = new GameController(gameService, objectMapper, userService);
        Controller<UserGame, Integer> userGameController = new UserGameController(userGameService, objectMapper, userService, gameService);

        Javalin app = Javalin.create(javalinConfig -> {
            javalinConfig.enableCorsForAllOrigins();
            javalinConfig.defaultContentType = "application/json";
            javalinConfig.prefer405over404 = true;
            javalinConfig.accessManager((handler, context, set) -> {
                Role userRole = getRole(context, userService);
                if (set.contains(userRole)) {
                    handler.handle(context);
                } else {
                    throw new ForbiddenResponse();
                }
            });
        });

        app.routes(() -> {
            path("users", () -> {
               get(userController::getAll, roles(UserRole.ADMIN));
               post(userController::postOne);
               path(":id", () -> {
                   get((ctx) -> userController.getOne(ctx, ctx.pathParam("id", Integer.class).get()), roles(UserRole.COMMON, UserRole.ADMIN));
                   patch((ctx) -> userController.patchOne(ctx, ctx.pathParam("id", Integer.class).get()), roles(UserRole.COMMON, UserRole.ADMIN));
                   delete((ctx) -> userController.deleteOne(ctx, ctx.pathParam("id", Integer.class).get()), roles(UserRole.COMMON, UserRole.ADMIN));
               });
            });

            path("games", () -> {
                get(gameController::getAll);
                post(gameController::postOne);
                path(":id", () -> {
                    get((ctx) -> gameController.getOne(ctx, ctx.pathParam("id", Integer.class).get()), roles(UserRole.COMMON, UserRole.ADMIN));
                    patch((ctx) -> gameController.patchOne(ctx, ctx.pathParam("id", Integer.class).get()), roles(UserRole.COMMON, UserRole.ADMIN));
                    delete((ctx) -> gameController.deleteOne(ctx, ctx.pathParam("id", Integer.class).get()), roles(UserRole.COMMON, UserRole.ADMIN));
                });
            });

            path("library", () -> {
                get(userGameController::getAll);
                path(":userId/:gameId", () -> post(userGameController::postOne));
                path(":id", () -> {
                    get((ctx) -> userGameController.getOne(ctx, ctx.pathParam("id", Integer.class).get()), roles(UserRole.COMMON, UserRole.ADMIN));
                    patch((ctx) -> userGameController.patchOne(ctx, ctx.pathParam("id", Integer.class).get()), roles(UserRole.COMMON, UserRole.ADMIN));
                    delete((ctx) -> userGameController.deleteOne(ctx, ctx.pathParam("id", Integer.class).get()), roles(UserRole.COMMON, UserRole.ADMIN));
                });
            });
        });
        app.start(7000);
    }
}
