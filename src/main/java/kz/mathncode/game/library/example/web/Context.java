package kz.mathncode.game.library.example.web;

public enum Context {
    GET,
    POST,
    PATCH,
    DELETE;
}
