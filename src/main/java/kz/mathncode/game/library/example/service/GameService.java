package kz.mathncode.game.library.example.service;

import com.j256.ormlite.dao.Dao;
import kz.mathncode.game.library.example.model.Game;

public class GameService extends AbstractService<Game, Integer> {
    public GameService(Dao<Game, Integer> dao) {
        super(dao);
    }
}
