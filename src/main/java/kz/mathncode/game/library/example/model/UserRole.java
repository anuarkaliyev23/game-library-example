package kz.mathncode.game.library.example.model;

import io.javalin.core.security.Role;

public enum UserRole implements Role {
    ADMIN,
    COMMON;
}
