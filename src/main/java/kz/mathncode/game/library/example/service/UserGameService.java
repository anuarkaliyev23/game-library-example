package kz.mathncode.game.library.example.service;

import com.j256.ormlite.dao.Dao;
import kz.mathncode.game.library.example.model.UserGame;

public class UserGameService extends AbstractService<UserGame, Integer> {
    public UserGameService(Dao<UserGame, Integer> dao) {
        super(dao);
    }
}
